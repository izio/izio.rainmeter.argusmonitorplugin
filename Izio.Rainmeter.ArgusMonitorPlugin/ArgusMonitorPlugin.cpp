#include <chrono>
#include <future>
#include <iostream>
#include <string>
#include <thread>
#include "argus_monitor_data_accessor.h"
#include "argus_monitor_data_api.h"
#include "../API/RainmeterAPI.h"

class DataAccess {
	static DataAccess* instance;
	argus_monitor::data_api::ArgusMonitorData monitor_data{};
	argus_monitor::data_api::ArgusMonitorDataAccessor data_accessor{};
	int children;

	DataAccess() {
		children = 0;

		auto const on_data_changed = [&](argus_monitor::data_api::ArgusMonitorData const& new_monitor_data) {

			try
			{
				monitor_data = new_monitor_data;
			}
			catch (const std::exception&)
			{
				RmLog(LOG_ERROR, L"Shit went wrong");
			}
		};

		data_accessor.RegisterSensorCallbackOnDataChanged(on_data_changed);
		data_accessor.Open();
	}

public:
	static DataAccess* getInstance() {
		if (!instance)
		{
			instance = new DataAccess;
		}

		if (!instance->data_accessor.IsOpen())
		{
			instance->data_accessor.Open();
		}

		return instance;
	}

	argus_monitor::data_api::ArgusMonitorData data() {
		return this->monitor_data;
	}

	bool isOpen() {
		return this->data_accessor.IsOpen();
	}

	void dispose() {
		this->data_accessor.Close();

		instance = 0;
	}
};

DataAccess* DataAccess::instance = 0;

enum SensorType
{
	SENSOR_TYPE_INVALID = 0,
	SENSOR_TYPE_TEMPERATURE,              // temperatures of mainboard sensors, external fan controllers and AIOs
	SENSOR_TYPE_SYNTHETIC_TEMPERATURE,    // user defined synthetic temperatures (mean, max, average, difference, ...)
	SENSOR_TYPE_FAN_SPEED_RPM,    // fan speed of fans attached to mainboard channels, AIOs, external fan controllers and also pump speeds of AIOs
	SENSOR_TYPE_FAN_CONTROL_VALUE,             // if any fan/pump is controlled by Argus Monitor, then the control value can be read from this
	SENSOR_TYPE_NETWORK_SPEED,                 // up/down speeds of network adapters if selected to be monitored inside Argus Monitor
	SENSOR_TYPE_CPU_TEMPERATURE,               // the normal CPU temperature readings, per core for Intel and the only one available for AMD
	SENSOR_TYPE_CPU_TEMPERATURE_ADDITIONAL,    // additional temperatures provided by the CPU, like CCDx temperatures of AMD CPUs
	SENSOR_TYPE_CPU_MULTIPLIER,       // multiplier value for every core (inside the CPU, those values are changed MUCH faster than the sampling  frequency)
	SENSOR_TYPE_CPU_FREQUENCY_FSB,    // core frequencies can be calculated by multiplying FSB frequency by the multipliers
	SENSOR_TYPE_GPU_TEMPERATURE,
	SENSOR_TYPE_GPU_NAME,    // the name of the GPU (e.g. "Nvidia RTX3080")
	SENSOR_TYPE_GPU_LOAD,
	SENSOR_TYPE_GPU_CORECLK,
	SENSOR_TYPE_GPU_MEMORYCLK,
	SENSOR_TYPE_GPU_SHARERCLK,
	SENSOR_TYPE_GPU_FAN_SPEED_PERCENT,
	SENSOR_TYPE_GPU_FAN_SPEED_RPM,
	SENSOR_TYPE_GPU_MEMORY_USED_PERCENT,
	SENSOR_TYPE_GPU_MEMORY_USED_MB,
	SENSOR_TYPE_GPU_POWER,
	SENSOR_TYPE_DISK_TEMPERATURE,
	SENSOR_TYPE_DISK_TRANSFER_RATE,
	SENSOR_TYPE_CPU_LOAD,
	SENSOR_TYPE_RAM_USAGE,
	SENSOR_TYPE_BATTERY,
	SENSOR_TYPE_MAX_SENSORS,
	SENSOR_TYPE_EVERYTHING
};

enum SensorDisplay
{
	VALUE,
	LABEL
};

struct Measure
{
	int sensor;
	SensorType type;
	SensorDisplay display;
	std::wstring label;

	Measure() : sensor(), type(SENSOR_TYPE_INVALID), display(VALUE) {};
};

PLUGIN_EXPORT void Initialize(void** data, void* rm)
{
	Measure* measure = new Measure;
	*data = measure;
}

PLUGIN_EXPORT void Reload(void* data, void* rm, double* maxValue)
{
	Measure* measure = (Measure*)data;

	//set sensor first and it can then be overridden
	measure->sensor = RmReadInt(rm, L"Sensor", 0);

	//get display mode
	LPCWSTR sensor_display = RmReadString(rm, L"SensorDisplay", L"");
	if (_wcsicmp(sensor_display, L"label") == 0)
	{
		measure->display = LABEL;
	}
	else {
		measure->display = VALUE;
	}

	//get type
	LPCWSTR sensor_type = RmReadString(rm, L"SensorType", L"");
	if (_wcsicmp(sensor_type, L"System_Temperature") == 0)
	{
		measure->type = SENSOR_TYPE_TEMPERATURE;
	}
	else if (_wcsicmp(sensor_type, L"Synthetic_Temperature") == 0)
	{
		measure->type = SENSOR_TYPE_SYNTHETIC_TEMPERATURE;
	}
	else if (_wcsicmp(sensor_type, L"Fan_Speed_Rpm") == 0)
	{
		measure->type = SENSOR_TYPE_FAN_SPEED_RPM;
	}
	else if (_wcsicmp(sensor_type, L"Fan_Control_Value") == 0)
	{
		measure->type = SENSOR_TYPE_FAN_CONTROL_VALUE;
	}
	else if (_wcsicmp(sensor_type, L"Network_Speed") == 0)
	{
		measure->type = SENSOR_TYPE_NETWORK_SPEED;
	}
	else if (_wcsicmp(sensor_type, L"CPU_Core_Temp") == 0)
	{
		measure->type = SENSOR_TYPE_CPU_TEMPERATURE;
	}
	else if (_wcsicmp(sensor_type, L"CPU_Additional_Temp") == 0)
	{
		measure->type = SENSOR_TYPE_CPU_TEMPERATURE_ADDITIONAL;
	}
	else if (_wcsicmp(sensor_type, L"CPU_Multiplier") == 0)
	{
		measure->type = SENSOR_TYPE_CPU_MULTIPLIER;
	}
	else if (_wcsicmp(sensor_type, L"CPU_Frequency_FSB") == 0)
	{
		measure->type = SENSOR_TYPE_CPU_FREQUENCY_FSB;
		measure->sensor = 0;
	}
	else if (_wcsicmp(sensor_type, L"CPU_Load_Total") == 0)
	{
		measure->type = SENSOR_TYPE_CPU_LOAD;
		measure->sensor = 0;
	}
	else if (_wcsicmp(sensor_type, L"CPU_Load_User") == 0)
	{
		measure->type = SENSOR_TYPE_CPU_LOAD;
		measure->sensor = 1;
	}
	else if (_wcsicmp(sensor_type, L"CPU_Load_System") == 0)
	{
		measure->type = SENSOR_TYPE_CPU_LOAD;
		measure->sensor = 2;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Name") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_NAME;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Temperature") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_TEMPERATURE;
		measure->sensor = 0;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Temperature_Spot") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_TEMPERATURE;
		measure->sensor = 1;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Temperature_Junction") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_TEMPERATURE;
		measure->sensor = 2;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Load") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_LOAD;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Core_Clock") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_CORECLK;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Memory_Clock") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_MEMORYCLK;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Memory_Used_Percent") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_MEMORY_USED_PERCENT;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Memory_Used_Mb") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_MEMORY_USED_MB;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Power") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_POWER;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Fan_Rpm") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_FAN_SPEED_RPM;
	}
	else if (_wcsicmp(sensor_type, L"GPU_Fan_Percent") == 0)
	{
		measure->type = SENSOR_TYPE_GPU_FAN_SPEED_PERCENT;
	}
	else if (_wcsicmp(sensor_type, L"Disk_Temperature") == 0)
	{
		measure->type = SENSOR_TYPE_DISK_TEMPERATURE;
	}
	else if (_wcsicmp(sensor_type, L"Disk_Transfer") == 0)
	{
		measure->type = SENSOR_TYPE_DISK_TRANSFER_RATE;
	}
	else if (_wcsicmp(sensor_type, L"RAM_Total") == 0)
	{
		measure->type = SENSOR_TYPE_RAM_USAGE;
		measure->sensor = 1;
	}
	else if (_wcsicmp(sensor_type, L"RAM_Used") == 0)
	{
		measure->type = SENSOR_TYPE_RAM_USAGE;
		measure->sensor = 2;
	}
	else if (_wcsicmp(sensor_type, L"RAM_Usage") == 0)
	{
		measure->type = SENSOR_TYPE_RAM_USAGE;
		measure->sensor = 0;
	}
	else if (_wcsicmp(sensor_type, L"Battery") == 0)
	{
		measure->type = SENSOR_TYPE_BATTERY;
	}
	else if (_wcsicmp(sensor_type, L"Everything") == 0)
	{
		measure->type = SENSOR_TYPE_EVERYTHING;
	}
	else
	{
		RmLog(rm, LOG_ERROR, L"Invalid \"Type\"");
	}
}

PLUGIN_EXPORT double Update(void* data)
{
	Measure* measure = (Measure*)data;

	DataAccess* data_access = data_access->getInstance();

	if (data_access->isOpen())
	{
		auto monitor_data = data_access->data();

		if (monitor_data.TotalSensorCount > 0)
		{
			auto const sensor_data_offset = monitor_data.OffsetForSensorType[measure->type];

			//ensure sensor value hasn't exceeded bounds of sensor type
			if (measure->sensor >= (int)monitor_data.SensorCount[measure->type])
			{
				measure->sensor = 0;
			}

			switch (measure->type)
			{
			case SENSOR_TYPE_TEMPERATURE:
			case SENSOR_TYPE_SYNTHETIC_TEMPERATURE:
			case SENSOR_TYPE_FAN_SPEED_RPM:
			case SENSOR_TYPE_FAN_CONTROL_VALUE:
			case SENSOR_TYPE_NETWORK_SPEED:
			case SENSOR_TYPE_CPU_TEMPERATURE:
			case SENSOR_TYPE_CPU_TEMPERATURE_ADDITIONAL:
			case SENSOR_TYPE_CPU_MULTIPLIER:
			case SENSOR_TYPE_CPU_FREQUENCY_FSB:
			case SENSOR_TYPE_GPU_TEMPERATURE:
			case SENSOR_TYPE_GPU_LOAD:
			case SENSOR_TYPE_GPU_CORECLK:
			case SENSOR_TYPE_GPU_MEMORYCLK:
			case SENSOR_TYPE_GPU_FAN_SPEED_PERCENT:
			case SENSOR_TYPE_GPU_FAN_SPEED_RPM:
			case SENSOR_TYPE_GPU_MEMORY_USED_PERCENT:
			case SENSOR_TYPE_GPU_MEMORY_USED_MB:
			case SENSOR_TYPE_GPU_POWER:
			case SENSOR_TYPE_DISK_TEMPERATURE:
			case SENSOR_TYPE_DISK_TRANSFER_RATE:
			case SENSOR_TYPE_CPU_LOAD:
			case SENSOR_TYPE_RAM_USAGE:
			case SENSOR_TYPE_BATTERY:
			{
				if (measure->display == LABEL)
				{
					measure->label = monitor_data.SensorData[sensor_data_offset + measure->sensor].Label;
				}
				else
				{
					return monitor_data.SensorData[sensor_data_offset + measure->sensor].Value;
				}

				break;
			}
			case SENSOR_TYPE_GPU_NAME:

				measure->label = monitor_data.SensorData[sensor_data_offset].Label;

				break;
			case SENSOR_TYPE_EVERYTHING:

				auto const sensor_data_count = monitor_data.TotalSensorCount;
				std::wstring output;

				for (std::size_t index{}; index < sensor_data_count; ++index) {

					output.append(monitor_data.SensorData[index].Label);
					output.append(L" - ");
					output.append(std::to_wstring(monitor_data.SensorData[index].SensorType));
					output.append(L" - ");
					output.append(std::to_wstring(monitor_data.SensorData[index].Value));
					output.append(L"\n");
				}

				measure->label = output;

				break;
			}
		}
	}

	return 0.0;
}

PLUGIN_EXPORT LPCWSTR GetString(void* data)
{
	Measure* measure = (Measure*)data;

	if (measure->display == LABEL || measure->type == SENSOR_TYPE_GPU_NAME)
	{
		return measure->label.c_str();
	}

	return nullptr;
}

//PLUGIN_EXPORT void ExecuteBang(void* data, LPCWSTR args)
//{
//	Measure* measure = (Measure*)data;
//}

//PLUGIN_EXPORT LPCWSTR (void* data, const int argc, const WCHAR* argv[])
//{
//	Measure* measure = (Measure*)data;
//	return nullptr;
//}

PLUGIN_EXPORT void Finalize(void* data)
{
	Measure* measure = (Measure*)data;
	DataAccess* data_access = data_access->getInstance();

	data_access->dispose();

	delete measure;
}